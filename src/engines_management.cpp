#include "engines_management.hh"

double _left, _right;

double enginePowerFromDirection();
void setEngineIOs();

void initEngines() {
    pinMode(IN1, OUTPUT);
    pinMode(IN2, OUTPUT);
    pinMode(IN3, OUTPUT);
    pinMode(IN4, OUTPUT);

    pinMode(PWM_RIGHT, OUTPUT);
    pinMode(PWM_LEFT, OUTPUT);

    digitalWrite(IN1, LOW);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, LOW);

    ledcSetup(PWM_CHAN_R, PWM_FREQ, PWM_RES);
    ledcSetup(PWM_CHAN_L, PWM_FREQ, PWM_RES);

    ledcAttachPin(PWM_RIGHT, PWM_CHAN_R);
    ledcAttachPin(PWM_LEFT, PWM_CHAN_L);
}

void updateEngineState() {
    // Hemisphère droit du joystick
    if(last_direction < 0) {
        _left = abs(last_direction) >= 90 ? 1 : -1;
        _right = enginePowerFromDirection();
    } 
    // Hemisphère gauche du joystick
    else {
        _right = abs(last_direction) >= 90 ? 1 : -1;
        _left = enginePowerFromDirection();
    }
    _right *= last_speed;
    _left *= last_speed;
    setEngineIOs();
}

double enginePowerFromDirection() {
    return (1.0/90.0) * abs(last_direction) - 1.0;
}

void setEngineIOs() {

    if(_right >= 0){
       digitalWrite(IN1, HIGH);
       digitalWrite(IN2, LOW);
   }
   else {
       digitalWrite(IN1, LOW);
       digitalWrite(IN2, HIGH);
   }

   if(_left >= 0){
       digitalWrite(IN4, HIGH);
       digitalWrite(IN3, LOW);
   }
   else {
       digitalWrite(IN4, LOW);
       digitalWrite(IN3, HIGH);
   }

    ledcWrite(PWM_CHAN_R, (int) 255 * abs(_right));
    ledcWrite(PWM_CHAN_L, (int) 255 * abs(_left));

}