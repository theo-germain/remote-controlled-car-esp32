#include "json_decoder.hh"

StaticJsonDocument<200> doc;

void decodeJsonPayload(char* payload) {
    DeserializationError error = deserializeJson(doc, payload);
    
    if(error) { 
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.f_str());
    }
    else if(strstr(payload, "isLedOn")) {
        (bool) doc["isLedOn"] ? turnOnLed() : turnOffLed();
    }
    else {
        last_speed = (double) doc["speed"];
        last_direction = (double) doc["direction"];

        #ifdef DEBUG
        Serial.print("speed : ");
        Serial.println((double) doc["speed"]);
        Serial.print("direction : ");
        Serial.println((double) doc["direction"]);
        #endif
        
        updateEngineState();
    }
}