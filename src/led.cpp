#include "led.hh"

void initLed() {
    pinMode(LED_PIN, OUTPUT);
    digitalWrite(LED_PIN, LOW);
}

void turnOnLed() {
    digitalWrite(LED_PIN, HIGH);
}

void turnOffLed() {
    digitalWrite(LED_PIN, LOW);
}