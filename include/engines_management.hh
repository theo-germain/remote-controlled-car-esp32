# pragma once

#include <Arduino.h>
#include "main.hh"
#include "engines_management.hh"

#define IN1 14
#define IN2 15
#define IN3 13
#define IN4 12

#define PWM_LEFT 3
#define PWM_RIGHT 2

#define PWM_FREQ 5000
#define PWM_RES 8

#define PWM_CHAN_R 9
#define PWM_CHAN_L 15

void initEngines();
void updateEngineState();