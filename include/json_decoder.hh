#pragma once

#include <ArduinoJson.h>
#include "engines_management.hh"
#include "led.hh"
#include "config.h"

void decodeJsonPayload(char* payload);
