#pragma once

#include <Arduino.h>
#include "esp_camera.h"
#include "udp_connection.hh"

#define CAMERA_MODEL_AI_THINKER

#include "camera_pins.h"

void initCamera();
void takePicture();

camera_fb_t* getFrameBuffer();
void freeFrameBuffer();