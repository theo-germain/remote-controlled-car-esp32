#pragma once

#include <Arduino.h>
#include "json_decoder.hh"
#include "config.h"

#define SERVER_NAME                 "Remote Controlled Car"

#define SERVICE_UUID                "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CAM_CHARACTERISTIC_UUID     "db27cb60-cdcf-4075-9be7-fef1621f34d7"
#define CTRL_CHARACTERISTIC_UUID    "07725e51-32f1-4303-adc7-9e4a4239048b"

void initBluetoothServer();
void modifyCameraServiceValue(uint8_t *buffer, size_t len);
void sendBluetoothImage(uint8_t *buffer, size_t len);
int getConnectedCount();